var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    jade = require('gulp-jade'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    nib = require('nib'),
    browserSync = require('browser-sync').create();

// Jade
gulp.task('jade', function(){
  gulp.src('src/*.jade') // Исходный файл
    .pipe(jade({pretty: true})) // Выводим красивый код
    .on('error', console.log) // Выводим ошибки в консоль
    .pipe(gulp.dest('build/')) // Файл на выходе
    .on('end', browserSync.reload);
});

// Stylus
gulp.task('stylus', function () {
  gulp.src('src/styl/*.styl') // Исходный файл
    .pipe(sourcemaps.init({loadMaps: true})) // запускаем sourcemaps
    .pipe(stylus({
      use: nib(),
      compress: false,
      'include css': true
    })) // Используем nib
    .pipe(sourcemaps.write()) // записываем soucemaps
    .on('error', console.log) // Выводим ошибки в консоль
    .pipe(gulp.dest('build/css/'))
    .on('end', browserSync.reload); // Файл на выходе
});

// добавление префиксов
gulp.task('autoprefix', function () {
  return gulp.src('css/charts.css')
    .pipe(autoprefixer('last 3 version', '> 1%', 'ie9'))
    .pipe(gulp.dest('css/'));
});


// webserver
gulp.task('webserver', function () {
  browserSync.init({
    server: {
      baseDir: './build/',
      tunnel: true
    }
  });
});

// Watch
gulp.task('watch', ['webserver'],function(){
  gulp.watch('src/styl/*.styl',['stylus']);
  gulp.watch('src/*.jade',['jade']);
});
