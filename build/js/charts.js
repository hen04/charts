$(function() {
	$('#chart-1').highcharts({
		title: {
			text: '',
			x: -20 //center
		},
		xAxis: {
			categories: ['00:00', '03:00', '06:00', '09:00', '12:00', '15:00',                '18:00', '21:00', '24:00'],
			crosshair: {
					width: 1,
					color: '#000',
					dashStyle: 'shortdot'
			},
			labels: {
				align: 'right',
				y: 28,
				x: -3,
				style: {
					fontSize: '15px',
					fontWeight: '400',
					color: '#767676'
				}
			}
		},
		yAxis: [{ // Primary yAxis
			title: {
				text: 'item',
				style: {
					color: '#fb8b1e'
				}
			},
			labels: {
				x: -5,
				y: 3,
				style: {
					color: '#fb8b1e',
					fontSize: '15px',
					fontWeight: '400'
				}
			}
		}, { // Secondary yAxis
			linkedTo: 0,
			opposite: true,
			title: {
				text: 'item',
				style: {
					color: '#668bfc'
				}
			},
			labels: {
				x: 5,
				y: 3,
				style: {
					color: '#668bfc',
					fontSize: '15px',
					fontWeight: '400'
				}
			},
		}],
		tooltip: {
			borderColor: '#979797',
			backgroundColor: '#eeeeee',
			useHTML: true,
			headerFormat: '<small>{point.key}</small>',
			pointFormat: '<p style="font-size: 12px; margin: 0; padding: 0;">{series.name}: </p>'
			+
			'<p style="font-size: 12px; margin: 0 0 5px; padding: 0;"><b>{point.y}</b></p>',
		},
		plotOptions: {
			series: {
				marker: {
					enabled: false,
				}
			}
		},
		series: [{
			name: 'Views in Central Region on MegaFon',
			data: [219, 61, 39, 55, 161, 177, 269, 278, 247 ],
			color: '#fb8b1e'
		}, {
			name: 'Stalls in Central Region on MegaFon',
			data: [296 , 82, 70, 86, 187, 241, 288, 296, 278 ],
			color: '#668bfc'
		}, {
			name: 'Views in Central Region on MTS ',
			data: [232 , 68, 28, 67, 160, 183, 258, 314, 360],
			dashStyle: 'dot',
			color: '#fb8b1e'
		}, {
			name: 'Stalls in Central Region on MTS ',
			data: [234 , 53, 22, 56, 141, 163, 300, 236, 381],
			dashStyle: 'dot',
			color: '#668bfc'
		}, {
			name: 'Views in North-West on MegaFon',
			marker: {
				enabled: false
			},
			data: [68 , 36, 16, 15, 48, 138, 134, 124, 74],
			dashStyle: 'dash',
			color: '#fb8b1e'
		}, {
			name: 'Stalls in North-West on MegaFon',
			data: [97 , 47, 27, 29, 56, 93, 147, 137, 120],
			dashStyle: 'dash',
			color: '#668bfc'
		}, {
			name: 'Views in North-West on MTS',
			data: [162 , 54, 29, 54, 132, 254, 256, 302, 332],
			dashStyle: 'longdash',
			color: '#fb8b1e'
		}, {
			name: 'Stalls in North-West on MTS',
			data: [235 , 90 , 41, 70, 141, 257, 263, 348, 332],
			dashStyle: 'longdash',
			color: '#668bfc'
		}]
	});




});

$(function(){
	$('.js-menu').on('click', function(){
		$(".js-single").select2();
		var level = $(this).next();
		var chbx = $(this).children().find('.label-input');
		console.log(chbx);
		if (level.css('display') == 'none') {
			level.show();
			chbx.addClass('active');
		} else {
			level.hide();
			chbx.removeClass('active');
		}
	});

	$('.label-input').on('click', function(){
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
		} else {
			$(this).addClass('active');
		}
	});

	$('.country-info > span').on('click', function(){
		var country = $(this).next();
		country.show();
	});

	$(".js-single").select2();

	$(".js-multiple").select2();

	var dateFormat = "mm/dd/yy",
	from = $( "#from" )
		.datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1
		})
		.on( "change", function() {
			to.datepicker( "option", "minDate", getDate( this ) );
		}),
	to = $( "#to" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1
	})
	.on( "change", function() {
		from.datepicker( "option", "maxDate", getDate( this ) );
	});

	function getDate( element ) {
	var date;
	try {
		date = $.datepicker.parseDate( dateFormat, element.value );
	} catch( error ) {
		date = null;
	}

	return date;
	}




});
